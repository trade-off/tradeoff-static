$(function()   {
    $(".faq-question").click(function() {
        $(this).next('div').slideToggle();
		
		$(this).toggleClass("link-background");
		$(this).toggleClass("pushed");
    });
 });
 
	$("#further-question, #further-question").click(function () {
	$(this).prevAll("#further-questions-content").slideToggle();
	$(this).toggleClass("further-questions-background");
	
    if ($.trim($(this).text()) === 'weitere Fragen' ) {
        $(this).text('weniger Anzeigen');
		$(this).css('color', '#fff');
		$(this).css('border-bottom','none');
    } else {	
        $(this).text('weitere Fragen');
		$(this).css('color', '#444');
		$(this).css('border-bottom','1px solid #e4e4e4');
    }
 });
